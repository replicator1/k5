import java.util.*;

public class Tnode {

   private String name;
   private Tnode firstChild;
   private Tnode nextSibling;

   private static final List<String> operators = Arrays.asList("+", "-", "*", "/");
   private static final List<String> specialOperators = Arrays.asList("DUP", "SWAP", "ROT");

   private final StringBuffer stringRepresentation = new StringBuffer();

   @Override
   public String toString() {
      buildString(this);
      return stringRepresentation.toString();
   }

   // https://www.geeksforgeeks.org/binary-tree-string-brackets/
   private void buildString(Tnode node) {

      stringRepresentation.append(node.name);

      if (node.firstChild == null) return;

      stringRepresentation.append("(");
      buildString(node.firstChild);
      if (operators.contains(node.name)) stringRepresentation.append(',');
      else stringRepresentation.append(")");


      if (node.firstChild.nextSibling != null)
      {
         buildString(node.firstChild.nextSibling);
         stringRepresentation.append(")");
      }
   }

   // https://stackoverflow.com/questions/19895308/converting-from-reverse-polish-notationrpn-into-a-tree-form
   public static Tnode buildFromRPN (String pol) {
      Stack<Tnode> stack = new Stack<>();

      for (String element: splitString(pol)){

         if (!operators.contains(element) && !specialOperators.contains(element)) {
            try {
               Integer.parseInt(element);
            } catch (NumberFormatException e) {
               throw new IllegalArgumentException("Expression: " + pol + " contains illegal symbol: " + element);
            }
         }

         Tnode node = new Tnode();
         node.name = element;

         if (operators.contains(element) || specialOperators.contains(element)) {
            Tnode a = new Tnode();
            Tnode b = new Tnode();
            Tnode c = new Tnode();

            try {
               switch (element){
                  case "DUP":
                     a = stack.pop();
                     break;
                  case "ROT":
                     a = stack.pop();
                     b = stack.pop();
                     c = stack.pop();
                     break;
                  default:
                     a = stack.pop();
                     b = stack.pop();
                     break;
               }

            } catch (EmptyStackException e){
               throw new IllegalArgumentException("Wrong expression: " + pol + " not enough elements");
            }

            if (specialOperators.contains(element)) {
               switch (element){
                  case "SWAP":
                     stack.push(a);
                     stack.push(b);
                     break;
                  case "DUP":
                     stack.push(a);

                     // copy of node
                     Tnode aCopy = cloneNode(a);
                     stack.push(aCopy);
                     break;
                  case "ROT":
                     stack.push(b);
                     stack.push(a);
                     stack.push(c);
                     break;
               }
            } else {
               b.nextSibling = a;
               node.firstChild = b;
            }

         }
         if (!specialOperators.contains(element)){
            stack.push(node);
         }
      }

      if (stack.size() != 1) throw new IllegalArgumentException("Wrong expression: " + pol + " too many elements ");
      return stack.pop();
   }

   private static List<String> splitString(String str) {
      String[] strSplit = str.split("\\s+");
      List<String> result = new ArrayList<>();

      for (String s : strSplit) {
         if (!s.isEmpty()) {
            result.add(s);
         }
      }
      return result;
   }

   private static Tnode cloneNode(Tnode node) {
      Tnode copy = new Tnode();
      copy.name = node.name;
      copy.firstChild = node.firstChild;
      copy.nextSibling = node.nextSibling;
      return copy;
   }

   public static void main (String[] param) {
//      String rpn = "1 2 +";
//      System.out.println ("RPN: " + rpn);
//      Tnode res = buildFromRPN (rpn);
//      System.out.println ("Tree: " + res);

//      String t = "5 1 - 7 * 6 3 / +";
//      Tnode r = buildFromRPN(t);
//      System.out.println("Result" + r.name + " :" + r.firstChild.nextSibling.firstChild.nextSibling.name);

//      String a = "+ ";
//      Tnode r1 = buildFromRPN(a);
//      System.out.println(r1.toString());

      String a = "2 5 DUP ROT - + DUP *";
      Tnode r1 = buildFromRPN(a);
      System.out.println(r1.toString());

   }
}

